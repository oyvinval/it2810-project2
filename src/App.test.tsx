import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import {unmountComponentAtNode} from "react-dom";

test('renders learn react link', () => {
  render(<BrowserRouter><App /></BrowserRouter>);
  const linkElement = screen.getByText(/If you think education is expensive, try ignorance./i);
  expect(linkElement).toBeInTheDocument();
});

it('renders Header without crashing', () => {
  const div = document.createElement('div');

  // @ts-ignore
  render(
      <BrowserRouter>
        <App />
      </BrowserRouter>,
      div);

  unmountComponentAtNode(div);
});
