import React, {Component} from 'react'
import {JokeData} from "../types/types";

interface MyProps {
    joke: JokeData
}

type MyState =  {
    revealed: boolean;
}

export class JokeCard extends Component<MyProps, MyState> {
    constructor(props: MyProps) {
        super(props);
    }

    state : MyState = {
        revealed: false
    }

    showPunchline = () => {
        let temp = this.state.revealed;
        this.setState({
            revealed: !temp
        })
    }

    render() {
        const {joke} = this.props;
        const {revealed} = this.state;
        return(
            <div>
                <div>{joke.setup}</div>
                { revealed ? <div>{joke.punchline}</div> : <button onClick={this.showPunchline}>Reveal punchline!</button>}
            </div>
        )
    }


}