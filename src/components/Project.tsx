import React, {useEffect} from 'react';
import {GitlabIssue, GitlabProject} from "../types/types";
import {issueService, projectService} from "../services/projectService";
import ProjectCard from "./ProjectCard";
import IssueCard from "./IssueCard";
import '../assets/css/project.css'


const Project = () => {

    const [project, setProject] = React.useState<GitlabProject | undefined>()
    const [issues, setIssues] = React.useState<GitlabIssue[] | null>()
    const [error, setError] = React.useState({})

    useEffect(() => {
        projectService
            .getProject()
            .then((data: any) => {
                console.log("project:", data)
                setProject(data)
            })
            .catch(error => setError({error}));
    }, []);

    useEffect(() => {
        issueService
            .getIssues()
            .then((data: any) => {
                console.log("issues: ", data)
                setIssues(data)
            })
            .catch(error => setError({error}));
    }, []);



    return (
        <div className="project-main">
        <h3>Projects</h3>
            { project ?  <ProjectCard project={project!}/> : null }

            <hr/>
        <h3>Issues</h3>
            { issues ?  <IssueCard issues={issues!}/> : null }
        </div>
    )
}

export default Project