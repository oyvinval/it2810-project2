import React, {useEffect} from 'react';
import '../assets/css/main.css';
import {bitcoinService, jokeService} from "../services/projectService";
import {BitCoinData, JokeData} from "../types/types";
import {Loading} from "./Loading";
import {JokeCard} from './JokeCard';

let frontpageImage = require('../assets/gfx/cover.jpg');

const Main = () => {

    const [bitcoinData, setBitcoinData] = React.useState<BitCoinData | undefined>()
    const [jokeData, setJokeData] = React.useState<JokeData | undefined>()
    const [error, setError] = React.useState({})

    useEffect(() => {
        bitcoinService
            .getBitcoinData()
            .then((data: any) => {
                setBitcoinData(data)
            })
            .catch(error => setError({error}));
        jokeService
            .getJoke()
            .then((data: any) => {
                setJokeData(data)
            })
            .catch(error => setError({error}));
    }, []);


    return (
        <div className="main-content">
            <div className="frontpage-img">
                <img src={frontpageImage.default} alt="img"/>
                <div className="image-text">“If you think education is expensive, try ignorance.”
                    <div className="image-text-quoted">- Andy McIntyre</div>
                </div>

            </div>

            <div className="div-padding-10 bit-cointainer">
                <h3>Bitcoin prices:</h3>
                {bitcoinData ? // Only show if object is bitcoinData is not empty

                    <ul className="bitcoin-prices">
                        <li>({bitcoinData.bpi.USD.code}) {bitcoinData.bpi.USD.rate}</li>
                        <li>({bitcoinData.bpi.EUR.code}) {bitcoinData.bpi.EUR.rate}</li>
                        <li>({bitcoinData.bpi.GBP.code}) {bitcoinData.bpi.GBP.rate}</li>
                        <li>Last updated: {bitcoinData.time.updated} </li>
                        <li>Disclaimer: {bitcoinData.disclaimer} </li>
                    </ul>

                    :
                    <Loading/>
                }
            </div>

            <div className="div-padding-10 bit-container">
                <h3>Joke of the day</h3>
                {jokeData ? // Only show if object is bitcoinData is not empty
                    <JokeCard joke={jokeData}/>
                    :
                    <Loading/>
                }
            </div>
        </div>
    )
}

export default Main;