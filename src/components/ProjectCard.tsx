import React from 'react';
import '../assets/css/projectCard.css';
import {GitlabProject} from '../types/types';

type ProjectCardProps = {
    project: GitlabProject;
}

const ProjectCard = ( { project }: ProjectCardProps) => {
    console.log(project)

    return(


            <div className="project-card">
                <div className="avatar"><img src={project?.avatar_url} alt="avatar" className="avatar_img"/></div>
                <div className="textarea">

                        <div className="project-name"><a href={project?.http_url_to_repo}>{project?.name}</a></div>
                        <div className="normal-text">Created: {project?.created_at}</div>
                        <div className="normal-text">Last activity: {project?.last_activity_at}</div>
                        <div className="normal-text">SSH URL: {project?.ssh_url_to_repo}</div>
                        <div className="normal-text">Star Count: {project?.star_count}</div>

                </div>


        </div>
    )
}

export default ProjectCard;