import React from "react";
import {Link} from "react-router-dom";
import '../assets/css/menu.css'

function Menu() {
    return(

        <div className="menu-bar">
            <div><Link to={"/"} className="nav-link">Home</Link></div>
            <div><Link to={"/project"} className="nav-link">Git Project</Link></div>
            <div><Link to={"/description"} className="nav-link">Project Description</Link></div>
        </div>
    )
}

export default Menu;
