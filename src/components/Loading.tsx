import React from 'react';
import '../assets/css/app.css'

export class Loading extends React.Component {
    render() {
        let loading = require('../assets/gfx/loading.svg');
        return <div className='loading-icon'><img src={loading.default} alt="Loading"/></div>;
    }
}