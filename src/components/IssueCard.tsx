import React from 'react';
import '../assets/css/projectCard.css';
import {GitlabIssue} from '../types/types';

type IssueCardProps = {
    issues: GitlabIssue[];
}

const IssueCard = ({issues}: IssueCardProps) => {
    console.log(issues)

    return (
        <>
            {
                issues.map((issue: GitlabIssue) => {
                    return (
                        <div className="issues-card">
                            <div className="textarea">
                                <div className="project-name"><a href={issue?.web_url}>{issue?.title}</a></div>
                                <div className="normal-text">Description: {issue?.description}</div>
                                <div className="normal-text">State: {issue?.state}</div>
                                <div className="normal-text">Created at: {issue?.created_at}</div>
                                <div className="normal-text">Updated at: {issue?.updated_at}</div>
                                <div className="normal-text">Author: {JSON.stringify(issue?.author)}</div>
                                <div className="normal-text">Assignee: {issue?.assignee}</div>
                                <div className="normal-text">Closed at: {issue?.closed_at}</div>
                                <div className="normal-text">Closed by: {issue?.closed_by}</div>
                            </div>
                        </div>
                    )
                })
            }
        </>
    )
}


let temp = [{
    "id": 51261,
    "iid": 1,
    "project_id": 11457,
    "title": "Skrive dokumentasjon",
    "description": "Prosjektet dokumenteres med en README.md i git repositoriet.\nDokumentasjonen skal diskutere, forklare og vise til alle de viktigste valgene og løsningene på krav til funksjonaltet og krav til teknologi. 800-1000 ord er en fornuftig lengde.\nGruppa skal oppsummere den enkeltes bidrag i prosjektet i en egen fil. Noter totalt antall timer og hva den enkelte har hatt som hovedbidrag. Denne leveres i BB (dette er personopplysninger som ingen vil at skal ligge på git ;-)",
    "state": "opened",
    "created_at": "2021-09-07T00:22:38.720+02:00",
    "updated_at": "2021-09-07T00:22:38.720+02:00",
    "closed_at": null,
    "closed_by": null,
    "labels": [],
    "milestone": null,
    "assignees": [],
    "author": {
        "id": 563,
        "name": "Øyvind Valstadsve",
        "username": "oyvinval",
        "state": "active",
        "avatar_url": "https://gitlab.stud.idi.ntnu.no/uploads/-/system/user/avatar/563/avatar.png",
        "web_url": "https://gitlab.stud.idi.ntnu.no/oyvinval"
    },
    "type": "ISSUE",
    "assignee": null,
    "user_notes_count": 0,
    "merge_requests_count": 0,
    "upvotes": 0,
    "downvotes": 0,
    "due_date": null,
    "confidential": false,
    "discussion_locked": null,
    "issue_type": "issue",
    "web_url": "https://gitlab.stud.idi.ntnu.no/oyvinval/it2810-project2/-/issues/1",
    "time_stats": {
        "time_estimate": 0,
        "total_time_spent": 0,
        "human_time_estimate": null,
        "human_total_time_spent": null
    },
    "task_completion_status": {"count": 0, "completed_count": 0},
    "has_tasks": false,
    "_links": {
        "self": "https://gitlab.stud.idi.ntnu.no/api/v4/projects/11457/issues/1",
        "notes": "https://gitlab.stud.idi.ntnu.no/api/v4/projects/11457/issues/1/notes",
        "award_emoji": "https://gitlab.stud.idi.ntnu.no/api/v4/projects/11457/issues/1/award_emoji",
        "project": "https://gitlab.stud.idi.ntnu.no/api/v4/projects/11457"
    },
    "references": {"short": "#1", "relative": "#1", "full": "oyvinval/it2810-project2#1"},
    "moved_to_id": null,
    "service_desk_reply_to": null
}]

export default IssueCard;