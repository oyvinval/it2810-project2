import React from 'react';
import '../assets/css/projectDescription.css';

const ProjectDescription = () => {
    return (
        <div className="project-description-content">
            <div className="vtbegenerated">
                <h2>Visualisering av gitlab data (tentativ beskrivelse)</h2>
                <div className="vtbegenerated_div">Innleveringsfrist fredag xx for prosjektet.
                    Gruppearbeid 3-4 personer.&nbsp;Prosjekt 2 teller 20% av
                    karakteren.<br/></div>

                <div className="vtbegenerated_div">I dette prosjektet skal du lage en
                    web-applikasjon (SPA) som presenterer informasjon om gitlab-repoet
                    til gruppa. Utvalgte data skal hentes fra gitlab med et REST API og
                    presenteres i en interaktiv webside hvor brukeren kan endre litt på
                    parametrene for visning eller valg av data.<br/>Dere bestemmer selv
                    hvilke data som hentes ut, men data data relatert til
                    commits og issues er grei utgangspunkt og eksempelvis kan
                    applikasjonen vise visualisere commits som er gjor i løpet
                    av en definert periode. Unngå å koble informasjon til
                    enkeltpersoner, eller anonymiser enkeltpersoner siden andre
                    studenter skal se på løsningen i medstudentvurderingen.
                </div>
                <div className="vtbegenerated_div">REST API til GitLab er dokumenter her: <a
                    href="https://docs.gitlab.com/ee/api/#rest-api">https://docs.gitlab.com/ee/api/#rest-api</a>
                </div>
                <div className="vtbegenerated_div">Oppgaven evalueres i forhold til krav og
                    læringsmålene som er beskrevet under.
                </div>
                <div className="vtbegenerated_div">Prosjektet leveres ved at koden er
                    tilgjengelig på gitlab og applikasjone skal installeres på en
                    virtuell maskin som gruppa får tilgang til. Fagstaben setter opp
                    repositories (vi må ha eierskap så vi kan sikre at prosjektet er
                    tilgjengelig også etter at undervisningen er ferdig).&nbsp;</div>
                <div className="vtbegenerated_div">Dere skal også levere en timeliste i BB
                    som dokumenterer den enkeltes innsats i prosjektet.
                </div>
                <h4 id="anonymous_element_10"><strong>Innhold og funksjonalitet</strong>
                </h4>
                <ul>
                    <li>Applikasjonen skal ha flere sideelementer, men utforming og valg
                        er opp til dere. Lag en løsning som dere mener er intuitiv
                        og fornuftig.
                    </li>
                    <li>Siden skal ha responsiv web-design (se under krav til teknologi). I praksis skal du demonstrere
                        bruk av teknikker for responsiv web design.
                    </li>
                    <li>En bruker skal kunne "lagre" noe lokalt, som skal hentes frem
                        (eksemeplvis lagre spesifikke innstillinger eller valg som
                        er gjort). I praksis skal du demonstrere bruk av local
                        storage.&nbsp;</li>
                    <li>Presentasjon av gitlab-data skal være parameterisert. Det vil si at en bruker skal kunne gjøre
                        noen valg som bidrar til å utforme/endre presentasjonen.
                    </li>
                </ul>
                <h4>Tekniske krav</h4>
                <ul>
                    <li>Løsningen implementeres med Typescript.</li>
                    <li>Løsningen skal baseres på React (og JSX),&nbsp;vis bruk av både
                        komponentene med class og funksjonelle komponenter.
                        Implementere en hesiktsmessig komponentstruktur.
                    </li>
                    <li>Bruk de ordinære mekanismene i React for å lagre og endre
                        tilstand. I prosjektet skal du vise bruke av props og state,
                        og vise bruk av Context API'et. <br/>Husk at du også skal
                        vise fornuftig bruk. (I dette prosjektet skal vi
                        ikke bruke løsninger som redux, mobx eller bibliotek
                        for å håndtere tilstand da dette er tema i neste
                        prosjekt).
                    </li>
                    <li>Du kan bruke UI-komponenter fra eksterne bibliotek, men skjal
                        også implementere egne Reackt komponenter
                    </li>
                    <li>Data fra GitLab skal lastes med AJAX (Asynchronous JavaScript).
                        Bruk fetch() eller velg tredjeparts javascript-bibliotek for
                        dette.
                    </li>
                    <li>I &nbsp;applikasjonen skal dere prøve ut og vise bruk av HTML Web Storage - både localstorage og
                        sessionstorage.
                        Det er fritt valg hvordan dere bruker Web Storage og til hva.
                    </li>
                    <li>Løsaningen skal ha responsiv web design hvor layout, skalering og interaksjonsmuligheter
                        tilpasses type enhet og størrelse på skjerm. Det skal se bra ut og interaksjonen skal fungere
                        både på mobil, pad og pc med skjerm av forskjellig størrelse.
                    </li>
                    <li>Følgende elementer skal være med i løsningen (eventuelt
                        begrunnet i dokumentasjonen hvis det ikke er tatt med)
                        <ul>
                            <li>Viewport</li>
                            <li>Media-queries</li>
                            <li>Bilder som skalerer</li>
                            <li>Flytende/fleksibel layout</li>
                        </ul></li>
                    <li>Prosjektet baseres på Node og bruk av Node Package Manager
                        (NPM). Bruk versjon 14.X av node.js og version 6.X av npm.
                    </li>
                    <li>Bruk create-react-app xxx --template typescript for å sette opp prosjektet</li>
                </ul>
                <div className="vtbegenerated_div"><strong>Testing</strong></div>
                <ul>
                    <li>Prosjektet
                        skal vise oppsett av og eksempel på testing med Jest -
                        minimum er å ha en snapshottest og noen enkle tester på
                        komponentenes oppførsel.&nbsp;<br/>Målet med dette kravet er at dere kommer i gang med testing,
                        får erfaring i oppsett og en forståelse av hva vi typisk tester i React-applikasjoner. Vi legger
                        lite vekt på omfanget av testingen, men dere skal vise at dere har forstått prinsippet i
                        testingen.
                    </li>
                    <li>Testing
                        av brukergrensesnitt og responsiv design:&nbsp;Gruppa skal beskrive/dokumentere testing på
                        minimum 3 forskjellige enheter hvor det må inngå en mobil (liten skjerm/horisontal + vertikal
                        orientering og en ordinær pc (stor skjerm).&nbsp;
                    </li>
                </ul>
                <h5><strong>&nbsp;</strong></h5>
                <strong>BRUK AV GIT, KODING</strong>
                <ul>
                    <li>Koden i prosjektet skal være ryddig strukturert, ha fornuftig
                        kommentering og ha navngiving av komponenter, variabler og
                        funksjoner i tråd med anbefalinger (best practise).
                    </li>
                    <li>Gruppa skal bruke git i utviklingen. Utviklingen skal
                        dekomponeres i task som hver beskrives kort med en issue.
                        Commits markeres med hvilken issue de bidrar til/løser. Vi
                        bruker gitlab og fagstaben setter opp repositories for
                        gruppene.&nbsp;</li>
                </ul>
                <h5 id="vtbegenerated_div">Dokumentasjon</h5>
                <ul>
                    <li>Prosjektet
                        dokumenteres med en README.md i git repositoriet.
                    </li>
                    <li>Dokumentasjonen skal diskutere, forklare og vise til alle de
                        viktigste valgene og løsningene på krav til funksjonaltet og
                        krav til teknologi. 800-1000 ord er en fornuftig lengde.
                    </li>
                    <li>Gruppa skal oppsummere den enkeltes bidrag i prosjektet i en
                        egen fil. Noter totalt antall timer og hva den enkelte har
                        hatt som hovedbidrag. Denne leveres i BB (dette er
                        personopplysninger som ingen vil at skal ligge på git ;-)
                    </li>
                </ul>

                <h6 id="vtbegenerated_div"><br/>LÆRINGSMÅL FOR PROSJEKTET</h6>
                <ul>
                    <li>Grunnleggende kunnskap og ferdigheter i React
                        (komponenter, JSX,&nbsp;&nbsp;props og state,
                        render, metoder relatert til komponentenes
                        livssyklus).
                    </li>
                    <li>Videregående kunnskap og ferdigheter i bruk av REST API
                        og ajax, hvordan benytte ajax i React.&nbsp;</li>
                    <li>Videregående kunnskap i responsiv web design. Skal
                        kjenne de viktigste teknikkene som brukes og har
                        erfaring i design og implementasjon.
                    </li>
                    <li>Kompetanse i bruk av git og dokumentasjon/styring av
                        utviklingsoppgaver med issues.&nbsp;</li>
                    <li>Grunnlegggende kunnskap og erfaring med oppsett og bruk
                        av node.js og npm.
                    </li>
                    <li>Ferdigheter i gruppearbeid med fokus på samarbeid og
                        læring, fordeling og koordinering av
                        arbeidsoppgaver, utvikling og samarbeidende
                        utvikling med git.
                    </li>
                    <li>Kunnskap om testing</li>
                </ul>

                <h5 id="anonymous_element_15">LEVERING
                </h5>
                <ul>
                    <li>Prosjektet leveres ved den versjonen som er
                        tilgjengelig på Gitlab ved
                        innleveringsfristen. Studenter som har behov
                        for utsettelse avtaler dette med fagstab. Vi
                        gir kun kortere utsettelse ved godt
                        begrunnede søknader (sykdom etc).
                    </li>
                    <li>Prosjektet settes til public eller internal
                        under evalueringen (også gruppen må settes
                        til public for at prosjekt skal kunne settes
                        til public).
                    </li>
                    <li>I oppgaven som legges ut på BB skal dere kun
                        levere timelisten (leveres her for å ivareta
                        personvern).
                    </li>
                </ul>
                <h5 id="anonymous_element_16">PLAGIATKONTROLL</h5>
                <div className="vtbegenerated_div">Kopiering og gjenbruk av
                    kode/løsninger som er utviklet av andre vil kun
                    godkjennes hvis det er gjort på en måte som
                    gir/viser læringsutbytte!<br/>I programmering er det
                    naturlig å la seg inspirere av andres
                    løsninger, gjenbruke fragmenter av kode etc.
                    Husk å dokumentere kilder så unngår du å bli
                    tatt for plagiering. Gjenbruk av løsninger
                    fra innleverte prosjekt tidligere år eller
                    andre former for omfattende kopiering,
                    regnes som fusk.
                </div>
            </div>
        </div>
    )
}


export default ProjectDescription;
