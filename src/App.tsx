import React from 'react';
import Main from "./components/Main";
import {Route, Switch} from "react-router-dom";
import Project from "./components/Project";
import Menu from "./components/Menu";
import ProjectDescription from "./components/ProjectDescription";
import './assets/css/app.css';

const App = () => {

    return (
        <div>
            <section className="header">
                <Menu/>
            </section>
            <section>
                <Switch>
                    <Route exact path="/" component={Main}/>
                    <Route path="/project" component={Project}/>
                    <Route path="/description" component={ProjectDescription}/>
                </Switch>
            </section>
            <div className="footer">
                Footer
            </div>
        </div>
    );
}

export default App;
