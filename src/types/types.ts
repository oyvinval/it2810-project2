export interface GitlabProject {
    avatar_url: string;
    created_at: string;
    http_url_to_repo: string;
    last_activity_at: string;
    readme_url: string;
    ssh_url_to_repo: string;
    name: string;
    star_count: number;
}

export interface PriceDetailData {
    code: string;
    symbol: string;
    rate: string;
    description: string;
    rate_float: number;
}

export interface PriceData {
    USD: PriceDetailData;
    GBP: PriceDetailData;
    EUR: PriceDetailData;
}

export interface BitcoinTime {
    updated: string;
    updatedISO: string;
    updateduk: string;
}



export interface BitCoinData {
    bpi: PriceData;
    chartName: string;
    disclaimer: string;
    time: BitcoinTime;
}

export interface JokeData {
    id: number;
    type: string;
    setup: string;
    punchline: string;
}

export interface GitlabIssue {
    title: string;
    description: string;
    state: string;
    created_at: string;
    updated_at: string;
    author: Object;
    assignee: string | null;
    closed_at: string | null;
    closed_by: string | null;
    web_url: string;

}
/*

assignee: null
assignees: []
author: {id: 563, name: "Øyvind Valstadsve", username: "oyvinval", state: "active", avatar_url: "https://gitlab.stud.idi.ntnu.no/uploads/-/system/user/avatar/563/avatar.png", …}
closed_at: null
closed_by: null
confidential: false
created_at: "2021-09-07T00:22:38.720+02:00"
description: "Prosjektet dokumenteres med en README.md i git repositoriet.\nDokumentasjonen skal diskutere, forklare og vise til alle de viktigste valgene og løsningene på krav til funksjonaltet og krav til teknologi. 800-1000 ord er en fornuftig lengde.\nGruppa skal oppsummere den enkeltes bidrag i prosjektet i en egen fil. Noter totalt antall timer og hva den enkelte har hatt som hovedbidrag. Denne leveres i BB (dette er personopplysninger som ingen vil at skal ligge på git ;-)"
discussion_locked: null
downvotes: 0
due_date: null
has_tasks: false
id: 51261
iid: 1
issue_type: "issue"
labels: []
merge_requests_count: 0
milestone: null
moved_to_id: null
project_id: 11457
references: {short: "#1", relative: "#1", full: "oyvinval/it2810-project2#1"}
service_desk_reply_to: null
state: "opened"
task_completion_status: {count: 0, completed_count: 0}
time_stats: {time_estimate: 0, total_time_spent: 0, human_time_estimate: null, human_total_time_spent: null}
title: "Skrive dokumentasjon"
type: "ISSUE"
updated_at: "2021-09-07T00:22:38.720+02:00"
upvotes: 0
user_notes_count: 0
web_url: "https://gitlab.stud.idi.ntnu.no/oyvinval/it2810-project2/-/issues/1"
_links: {self: "https://gitlab.stud.idi.ntnu.no/api/v4/projects/11457/issues/1", notes: "https://gitlab.stud.idi.ntnu.no/api/v4/projects/11457/issues/1/notes", award_emoji: "https://gitlab.stud.idi.ntnu.no/api/v4/projects/11457/issues/1/award_emoji", project: "https://gitlab.stud.idi.ntnu.no/api/v4/projects/11457"}
*/
