import {BitCoinData, GitlabIssue, GitlabProject, JokeData} from '../types/types';
import axios, {AxiosResponse} from 'axios';

axios.interceptors.response.use(response => response.data);

const corsURL = 'https://vast-mountain-00232.herokuapp.com/';
const bitcoinURL = 'https://api.coindesk.com/v1/bpi/currentprice.json';
const projectURL = 'https://gitlab.stud.idi.ntnu.no/api/v4/projects/11457';
const issueURL = 'https://gitlab.stud.idi.ntnu.no/api/v4/projects/11457/issues';
const jokeURL = 'https://official-joke-api.appspot.com/random_joke';

let axiosConfig = {
    headers: {
        "Content-Type": "application/json; charset=utf-8",
        "x-access-token": sessionStorage.getItem("storedtoken")
    }
};

class ProjectService {
    getProject(): Promise<AxiosResponse> {
        return axios.get<GitlabProject>(projectURL, axiosConfig);
    }
}

class IssueService {
    getIssues(): Promise<AxiosResponse> {
        return axios.get<GitlabIssue[]>(issueURL, axiosConfig);
    }
}

class BitcoinService {
    getBitcoinData(): Promise<AxiosResponse> {
        return axios.get<any>(corsURL + bitcoinURL, axiosConfig);
    }
}


class JokeService {
    getJoke(): Promise<AxiosResponse> {
        return axios.get<JokeData>(corsURL + jokeURL, axiosConfig);
    }
}


export let jokeService = new JokeService();
export let issueService = new IssueService();
export let projectService = new ProjectService();
export let bitcoinService = new BitcoinService();
